// s54
import React from 'react';

// Creates a Context Object
// A context obj with obj data type that can be used to store info that can be share to other components within the application
const UserContext = React.createContext();
//console.log("Content of UserContext");
//console.log(UserContext);

// The "Provider" component allows other compponents to consume/use the context object and supply the necessary info needed in the context object
// the provider is used to create a context that can be consumed 
export const UserProvider = UserContext.Provider;

export default UserContext;