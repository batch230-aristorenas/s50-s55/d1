/*
  option 1 preparations:
  - npx create-react-app appName
  - npm start
  - npm install react-bootstrap bootstrap

  option 2 preparations
  - npm install -g create-react-app
  - create-react-app appName
  - npm start
  - npm install react-bootstrap bootstrap
  -npm i sweetalert2(for alert)
*/

import React from 'react';
import ReactDOM from 'react-dom/client';

//remove
//import './index.css';

import App from './App';

//remove
//import reportWebVitals from './reportWebVitals';

import 'bootstrap/dist/css/bootstrap.min.css';

// We need to create a root to display React components

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);


/*
const name = 'John Smith';
const user = {
  firstName : 'Jane',
  lastName : 'Smith'
}

function formatName(parameterUser){
  return parameterUser.firstName + ' ' + parameterUser.lastName;
}

const element = <h1>Hello, {formatName(user)}</h1>

  // We need to create a root to display react components
  // Reference: https://beta.reactjs.org/reference/react-dom/client/createRoot
const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(element)
*/

