// import Button from 'react-bootstrap/Button';
// import Card from 'react-bootstrap/Card';
import { Button, Card } from 'react-bootstrap';
import { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import {Link} from 'react-router-dom';

/*export default function CourseCard(props) {

  console.log("Content of props: ");
  console.log(props);
  console.log(typeof props);

  return (
    <Card>
      
      <Card.Body>
        <Card.Title>{props.courseProp.name}</Card.Title>
        <Card.Subtitle>Description:</Card.Subtitle>
        <Card.Text>{props.courseProp.description}</Card.Text>
        <Card.Subtitle>Price:</Card.Subtitle>  
        <Card.Text>{props.courseProp.price}</Card.Text>
        <Button variant="primary">Enroll</Button>
      </Card.Body>
    </Card>
  );
}*/

export default function CourseCard({courseProp}) {

  // console.log("Content of props: ");
  // console.log(props);
  // console.log(typeof props);

  const {_id,name, description, price} = courseProp;

  // State Hooks (useState) - a way to store info within a component and track this info
        //getter, setter
        //variable, function to change the value of a variable
  // const [count, setCount] = useState(0); // count = 0
  // const [seats, setSeats] = useState(30); // seat = 30


  // function enroll(){
    

    /*if (seats <= 0) {
      alert("No more seats available");
    }
    else {
      setCount(count + 1);
      setSeat(seats - 1);
    }*/


  //   console.log("Enrollees: " + count);
  //   console.log("Seats: " + seats);
  //   setCount(count + 1);
  //     setSeats(seats - 1);
  // }

  // useEffect() always run the task on the initial render and/or every render (when the state changes in the component)
  // Initial render is when the component is run or displayed for the first time
  // useEffect(() => {
  //   if(seats === 0){
  //     alert("No more seats available");
  //   }
  // }, [seats]);


  return (
    <Card>
      
      <Card.Body>
        <Card.Title>{name}</Card.Title>
        <Card.Subtitle>Description:</Card.Subtitle>
        <Card.Text>{description}</Card.Text>
        <Card.Subtitle>Price:</Card.Subtitle>  
        <Card.Text>{price}</Card.Text>
        <Button as={Link} to={`/courses/${_id}`}>Enroll</Button>
        {/* <Card.Text>Total Enrolled: {count}<br/>Seats available: {seats}</Card.Text> */}
      </Card.Body>
    </Card>
  )
}

// Check if the CourseCard component is getting the correct property types
// CourseCard.propTypes = {
//   courseProp: PropTypes.shape({
//     name: PropTypes.string.isRequired,
//     description: PropTypes.string.isRequired,
//     price: PropTypes.number.isRequired
//   })
// }