//remove
//import logo from './logo.svg';
import './App.css';
import AppNavbar from './components/AppNavbar';

// import Home from './pages/Home';
// import Courses from './pages/Courses';


//import Banner from './components/Banner';
//import Highlights from './components/Highlights';

import { Container } from 'react-bootstrap';

// react Fragment allows us to return multiple elements
//import { Fragment } from 'react'; // s53 comment
import { BrowserRouter as Router } from 'react-router-dom'; //s53 added
import { Route, Routes } from 'react-router-dom'; // s53 added

import Home from './pages/Home';
import Courses from './pages/Courses';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Error from './pages/Error';
import CourseView from './pages/CourseView';

//s54
import Settings from './pages/Settings';
import { UserProvider } from './UserContext'
// s55
import { useState, useEffect, useContext } from 'react'; 


function App() {

  // 1 - create state
  const [user,setUser] = useState({
    email: localStorage.getItem('email')
  })

  const unsetUser = () => {
    localStorage.clear();
  }
  return (
    // react fragments allows us to retun multiple elements

    // 2 - provide/share the state to other components
    <UserProvider value = {{user,setUser, unsetUser}}> {/*s54*/}
    <Router>
      <Container fluid>
        <AppNavbar />
        <Routes>
          <Route path='/' element={<Home />} />
          <Route path='/courses' element={<Courses />} />
          <Route path='/courses/:courseId' element={<CourseView />} />
          <Route path='/register' element={<Register />} />
          <Route path='/login' element={<Login />} />
          <Route path='/logout' element={<Logout />} />
          <Route path='/settings' element={<Settings />} />         
          <Route path='/*' element={<Error />} />
        </Routes>       
      </Container>
    </Router>
    </UserProvider >

    
  );
}
/*
      <Router>
        <Routes>
          <Route path="/yourDesiredPath" element { <Component /> } />
        <Routes>
      </Router>
*/
export default App;
