import { Form, Button } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import UserContext from '../UserContext';
import { Navigate } from 'react-router-dom';

import Swal from 'sweetalert2';
export default function Login() {

	// s54
	// variable, setter function
	// 3 - Use the context
	const { user,setUser } = useContext(UserContext);

	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [isActive, setIsActive] = useState(false);

	console.log(email);
	console.log(password);

	// function loginUser(event){

	// 	event.preventDefault();

	// 	localStorage.setItem('email', email);

	// 	setUser({
	// 		email: localStorage.getItem('email', email)
	// 	}) //54

	// 	setEmail('');
	// 	setPassword('');
	// 	alert('You are now logged in.');
	// }
function authenticate(e){
	e.preventDefault();

	fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
		method: 'POST',
		headers: { 'Content-Type' : 'application/json'},
		body: JSON.stringify({
			email: email,
			password: password
		})
	})
	.then(res => res.json())
	.then(data => {
		console.log(data);
		console.log("Check accessToken");
		console.log(data.accessToken);
		

		if(typeof data.accessToken !== "undefined"){
			localStorage.setItem('token',data.accessToken);
			retrieveUserDetails(data.accessToken);

			Swal.fire({
				title: "Login Successful",
				icon: "success",
				text: "Welcome to Zuitt"
			})
		}
		else{
			Swal.fire({
				title: "Authentication failed",
				icon: "error",
				text: "Check your login details and try again."
			})
		}

	})

	setEmail('');
}

const retrieveUserDetails = (token) => {
	fetch(`${process.env.REACT_APP_API_URL}/users/details`,{
		headers: {
			Authorization:`Bearer ${token}`
		}
	})
	.then(res => res.json())
	.then(data => {
		console.log(data);

		setUser({
			//email: data.email,
			id: data._id,
			isAdmin: data.isAdmin
		})
	})
}

	useEffect(() => {
		if(email !== '' && password !== ''){
			setIsActive(true);
		}
		else{
			setIsActive(false);
		}
	}, [email, password])


	return (
		//s54
		(user.id !== null)
		?// true - means email field is successfully set
		<Navigate to="/courses" />
		: // false - means email field is not successfully set

		<Form onSubmit ={(event) => authenticate(event)}>
		<h3>Login</h3>
            <Form.Group controlId="userEmail">
                <Form.Label>Email address</Form.Label>
                <Form.Control 
	                type="email" 
	                placeholder="Enter email" 
	                //to save the value of email
	                value = {email}
	                // event
	                onChange = {event => setEmail(event.target.value)}

	                required
                />
                <Form.Text className="text-muted">
                    We'll never share your email with anyone else.
                </Form.Text>
            </Form.Group>

            <Form.Group controlId="password">
                <Form.Label>Password</Form.Label>
                <Form.Control 
	                type="password" 
	                placeholder="Password" 
	                //to save the value of password1
	                value = {password}
	                // event
	                onChange = {event => setPassword(event.target.value)}
	                required
                />
            </Form.Group>
            { isActive ? // true or if
	            <Button variant="primary" type="submit" id="submitBtn">
	            	Submit
	            </Button>
	            : // false or else
	            <Button variant="secondary" type="submit" id="submitBtn" disabled>
            		Submit
            	</Button>
            }      
        </Form>
	)
}