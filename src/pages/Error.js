


/*export default function Error(){

	return(
		<div>
			<h1>Page Not Found</h1>
			<h3>Go back to the <a href="/">homepage</a></h3>
		</div>
	)
}*/


// s53 Error page activity solution
import Banner from '../components/Banner';

export default function Error(){

	const data = {
		title: "404 - Not found",
		content: "The page you are looking for cannot be found",
		destination: "/",
		label: "Back home"
	}

	return (
		<Banner data={data}/>
	)
}