import { Form, Button, Container, Col, Row } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import UserContext from '../UserContext';
import { Navigate } from 'react-router-dom';
import {useParams, useNavigate, Link} from 'react-router-dom';
import Swal from "sweetalert2";

export default function Register(){

	const { user } = useContext(UserContext);
	const navigate = useNavigate();
	//State hooks to store the values of input fields
	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [email, setEmail] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');
	const [mobileNumber, setMobileNumber] = useState('');

	// State to determine whether submit button is enabled or not
	const [isActive, setIsActive] = useState(false);

	// s54 Activity
	//const { user,setUser } = useContext(UserContext);

	// Check if values are successfully binded
	console.log(firstName);
	console.log(lastName);
	console.log(email);
	console.log(password1);
	console.log(password2);
	console.log(mobileNumber);



function registerUser(event){

		// Prevents page redirection via form submission
		//event.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
			method: "POST",
            headers: {
                "Content-Type" : "application/json"
            },
            body: JSON.stringify({
                firstName : firstName,
				lastName : lastName,
				email : email,
				password : password1,
				mobileNumber : mobileNumber
            })
		})
			.then(res => res.json())
			.then(data => {
			console.log(data);

				if(data){
					Swal.fire({
						title: "Registration Successfull",
						icon: "success",
						text: "Welcome to Zuitt."
					})
					navigate("/courses")
				}
				else{
					Swal.fire({
						title: "Duplicate email found",
						icon: "error",
						text: "Please provide a different email."
					})
				}
			})
	
		// Clear input fields
		// setFirstName('');
		// setLastName('');
		// setEmail('');
		// setPassword1('');
		// setPassword2('');
		// setMobileNumber('');
		//alert('Thank you for registering');
}
	useEffect(() => {
		if((firstName !== '' && lastName !== '' && email !== '' && password1 !== '' && password2 !== '' && mobileNumber !== '') && (password1 === password2)){setIsActive(true);}
		else {
			setIsActive(false);
		}
	}, [firstName, lastName, email, password1, password2, mobileNumber])


	return(

		// s54 activity
		(user.id !== null)
		?// true - means email field is successfully set
		<Navigate to="/courses" />
		: // false - means email field is not successfully set

		<Form onSubmit ={(event) => registerUser(event)}>
		<h3>Register</h3>
			<Form.Group controlId="firstName">
                <Form.Label>First Name</Form.Label>
                <Form.Control 
	                type="text" 
	                placeholder="Enter First Name" 
	                //to save the value of email
	                value = {firstName}
	                // event
	                onChange = {event => setFirstName(event.target.value)}
	                required
                />
            </Form.Group>

			<Form.Group controlId="lastName">
                <Form.Label>Last Name</Form.Label>
                <Form.Control 
	                type="text" 
	                placeholder="Enter Last Name" 
	                //to save the value of email
	                value = {lastName}
	                // eventa
	                onChange = {event => setLastName(event.target.value)}
	                required
                />
            </Form.Group>

            <Form.Group controlId="userEmail">
                <Form.Label>Email address</Form.Label>
                <Form.Control 
	                type="email" 
	                placeholder="Enter email" 
	                //to save the value of email
	                value = {email}
	                // event
	                onChange = {event => setEmail(event.target.value)}

	                required
                />
                <Form.Text className="text-muted">
                    We'll never share your email with anyone else.
                </Form.Text>
            </Form.Group>

            <Form.Group controlId="password1">
                <Form.Label>Password</Form.Label>
                <Form.Control 
	                type="password" 
	                placeholder="Password" 
	                //to save the value of password1
	                value = {password1}
	                // event
	                onChange = {event => setPassword1(event.target.value)}
	                required
                />
            </Form.Group>

            <Form.Group controlId="password2">
                <Form.Label>Verify Password</Form.Label>
                <Form.Control 
	                type="password" 
	                placeholder="Verify Password" 
	                //to save the value of password2
	                value = {password2}
	                // event
	                onChange = {event => setPassword2(event.target.value)}
	                required
                />
            </Form.Group>

			<Form.Group controlId="mobileNumber">
                <Form.Label>Mobile Number</Form.Label>
                <Form.Control 
	                type="number" 
	                placeholder="Enter Mobile Number" 
	                //to save the value of email
	                value = {mobileNumber}
	                // event
	                onChange = {event => setMobileNumber(event.target.value)}
	                required
                />
            </Form.Group>
            { isActive ? // true or if
	            <Button variant="primary" type="submit" id="submitBtn">
	            	Submit
	            </Button>
	            : // false or else
	            <Button variant="secondary" type="submit" id="submitBtn" disabled>
            	Submit
            	</Button>

            }
            
        </Form>

	)
}